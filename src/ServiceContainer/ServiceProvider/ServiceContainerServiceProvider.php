<?php

/**
 * @file
 * Contains \Drupal\event_dispatcher\ServiceContainer\ServiceProvider\ServiceContainerServiceProvider
 */

namespace Drupal\event_dispatcher\ServiceContainer\ServiceProvider;

use Drupal\service_container\DependencyInjection\ServiceProviderInterface;

/**
 * Provides render cache service definitions.
 */
class ServiceContainerServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function getContainerDefinition() {
    return array(
      'parameters' => [],
      'services' => [],
    );
  }

  /**
   * {@inheritdoc}
   */
  public function alterContainerDefinition(&$container_definition) {
    if (isset($container_definition['tags']['event_subscriber'])) {
      foreach (array_keys($container_definition['tags']['event_subscriber']) as $service_id) {
        $class = $container_definition['services'][$service_id]['class'];
        $container_definition['services']['event_dispatcher']['calls'][] = ['addSubscriberService', [$service_id, $class]];
      }
    }
  }
}
