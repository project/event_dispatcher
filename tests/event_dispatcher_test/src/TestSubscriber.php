<?php

namespace Drupal\event_dispatcher_test;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class TestSubscriber implements EventSubscriberInterface{

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return ['event_dispatcher_test' => 'onTest'];
  }

  function onTest() {
    watchdog('event_dispatcher_test', __CLASS__ . ':' . __FUNCTION__);
  }
}
